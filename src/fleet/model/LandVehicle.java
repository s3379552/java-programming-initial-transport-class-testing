package fleet.model;

public abstract class LandVehicle extends AbstractVehicle {

	private double odometer;
	private LandMaintenanceInfo landMinfo;
	
	public LandVehicle(String make, String model, String regNo, int year, double serviceInterval) {
		super(make, model, regNo, year);
		this.odometer = 0.0;
		landMinfo = new LandMaintenanceInfo(this, serviceInterval);
	}
	
	public boolean canTravel(double distance){
		if((odometer + distance) < landMinfo.getNextServicePoint())
			return true;
		return false;
	}
	
	public void service(){
	}
	
	public double getOdometer(){
		return odometer;
	}
	
	public double travel(double distance){
		odometer += distance;
		return odometer;
	}
	
	@Override
	public String toString() {
		return "odometer=" + odometer + ", serviceInterval=" + landMinfo.getServiceInterval() + ", landMinfo=" + landMinfo;
	}
}
