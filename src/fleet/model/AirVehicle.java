package fleet.model;

public abstract class AirVehicle extends AbstractVehicle {

	public static final int FLIGHTS_PER_JOB = 1;
	private int flightCount;
	private double flightHours;
	private double averageSpeed;
	private AirMaintenanceInfo airMInfo;
	
	public AirVehicle(String make, String model, String regNo, int year, double averageSpeed, 
			int siFlightCountInterval, double siFlightHoursInterval) {
		super(make, model, regNo, year);
		this.averageSpeed = averageSpeed;
		flightCount = 0; 
		airMInfo = new AirMaintenanceInfo(this, siFlightCountInterval, siFlightHoursInterval);
		flightHours = 0.0;
	}
	
	public int getFlightCount(){
		return flightCount;
	}
	
	public double getFlightHours(){
		return flightHours;
	}
	
	public double getAverageSpeed(){
		return averageSpeed;
	}
	
	public void setAverageSpeed(double aveSpeed){
		averageSpeed = aveSpeed;
	}
	
	public boolean canTravel(double distance){
		if(flightHours + estimateFlyingHours(distance) < airMInfo.getServiceIntervalFlightHours())
			return true;
		else if(flightCount < FLIGHTS_PER_JOB)
			return true;
		return false;
	}
	
	public double estimateFlyingHours(double distance){
		return distance/averageSpeed;
	}
	
	public double travel(double distance){
		flightHours += estimateFlyingHours(distance);
		flightCount += 1;
		return flightHours;
	}
	
	public void service(){
		flightHours = 0.0;
		flightCount = 0;
	}
	
	public String toString(){
		return "make = " + getMake() + ", model = " + getModel() + ", regNo = " + getRegNo() + ". year = " + getYear() + ", average Speed = " + averageSpeed;
	}
	
	
	
}
