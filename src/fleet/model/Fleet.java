package fleet.model;

import java.util.ArrayList;

public class Fleet {
	private ArrayList<Vehicle> vehicles;
	private ArrayList<Job> jobs;
	
	public Fleet() {}
	
	public boolean addVehicle(Vehicle v){
		if(vehicles.contains(v) == true)
			return false;
		vehicles.add(v);
		return true;
	}
	public boolean removeVehicle(Vehicle v){
		if(vehicles.contains(v) == false)
			return false;
		vehicles.remove(v);
		return true;
	}
	
	public void displayAllVehicles(){
		for(int i = 0; i < vehicles.size(); i++){
			System.out.println(vehicles.get(i).toString());
			}
	}
	public void displayAllJobs(){
		for(int i = 0; i < jobs.size(); i++){
			System.out.println(jobs.get(i).toString());
		}
	}
	
	public void displayVehicleInfo(String regNo){
		for(int i = 0; i < vehicles.size(); i++){
			if(vehicles.get(i).getRegNo() == regNo){
				System.out.println(vehicles.get(i).toString());
			}
		}
	}
	
	public void displayJobInfo(String jobID){
		for(int i = 0; i < jobs.size(); i++){
			if(jobs.get(i).getJobID() == jobID){
				System.out.println(jobs.get(i).toString());
			}
		}
	}
	
	public Vehicle getVehicle(String regNo){
		for(int i = 0; i < vehicles.size(); i++){
			if(vehicles.get(i).getRegNo() == regNo){
				return vehicles.get(i);
			}
		}
		return null;
	}
	
	public Job getJob(String jobID){
		for(int i = 0; i < jobs.size(); i++){
			if(jobs.get(i).getJobID() == jobID){
				return jobs.get(i);
			}
		}
		return null;
	}
	
	public void serviceVehicle(String regNo){
		for(int i = 0; i < vehicles.size(); i++){
			if(vehicles.get(i).getRegNo() == regNo){
				vehicles.get(i).service();
			}
		}
	}
	
}
