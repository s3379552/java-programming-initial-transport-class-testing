package fleet.model;

public class Aircraft extends AirVehicle {

	public static final int WEAR_AND_TEAR_FLAGFALL = 30000;
	private final double WEAR_AND_TEAR_RATE = 5;
	public Aircraft(String make, String model, String regNo, int year,
			double averageSpeed, int siFlightCountInterval,
			double siFlightHoursInterval) {
		super(make, model, regNo, year, averageSpeed, siFlightCountInterval,
				siFlightHoursInterval);
	}
	public double calculateWearAndTear(double distance){
		return WEAR_AND_TEAR_FLAGFALL + (WEAR_AND_TEAR_RATE * distance);
	}

}
