package fleet.model;

public class Truck extends LandVehicle {

	public static final double WEAR_AND_TEAR_RATE = 0.5;
	private double loadCapacity;
	
	public Truck(String make, String model, String regNo, int year,
			double serviceInterval, double loadCapacity) {
		super(make, model, regNo, year, serviceInterval);
		this.loadCapacity = loadCapacity;
	}
	
	public double calculateWearAndTear(double distance){
		return distance * (WEAR_AND_TEAR_RATE * loadCapacity);
	}
	
	public double getLoadCapacity(){
		return loadCapacity;
	}

	@Override
	public String toString() {
		return "make = " + getMake() + ", model = " + getModel() + ", year = " + getYear() + ", regNo = " + getRegNo() + ", odometer = " + getOdometer()
				+ ", loadCapacity = " + loadCapacity;
	}
	
	

}
