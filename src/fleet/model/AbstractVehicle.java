package fleet.model;

public abstract class AbstractVehicle implements Vehicle {

	private String make;
	private String model;
	private String regNo;
	private int year;
	
	
	public AbstractVehicle(String make, String model, String regNo, int year) {
		this.make = make;
		this.model = model;
		this.regNo = regNo;
		this.year = year;
	}

	
	public String getMake() {
		return make;
	}

	public String getModel() {
		return model;
	}

	public int getYear() {
		return year;
	}

	public double calculateWearAndTear(double distance) {
		// Method implemented in subclass anyway
		return 0;
	}

	public String getRegNo() {
		return regNo;
	}

	public void service() {
		// method implemented in subclass anyway

	}

	public boolean canTravel(double distance) {
		// method implemented in subclass anyway
		return false;
	}

	public double travel(double distance) {
		// method implemented in subclass anyway
		return 0;
	}


	public String toString() {
		return "make=" + make + ", model=" + model + ", regNo=" + regNo + ", year=" + year;
	}
	
	public boolean equals(Object o){
		
		if (o instanceof LandVehicle || o instanceof AirVehicle)
			return true;
		return false;
	}

}
