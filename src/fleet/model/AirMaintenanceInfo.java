package fleet.model;

public class AirMaintenanceInfo implements MaintenanceInfo {

	private AirVehicle aircraft;
	private int lastSPFlightCount;
	private int lastSPFlightHours;
	private final int SERVICE_INTERVAL_FLIGHT_COUNT;
	private final double SERVICE_INTERVAL_FLIGHT_HOURS;
	
	public AirMaintenanceInfo(AirVehicle vehicle, int serviceFC, double serviceFH){
		
		SERVICE_INTERVAL_FLIGHT_COUNT = serviceFC;
		SERVICE_INTERVAL_FLIGHT_HOURS = serviceFH;
		aircraft = vehicle;
	}
	
	
	public AirVehicle getVehicle() {
		return aircraft;
	}


	public int getLastSPFlightCount() {
		return lastSPFlightCount;
	}


	public int getLastSPFlightHours() {
		return lastSPFlightHours;
	}


	public int getServiceIntervalFlightCount() {
		return SERVICE_INTERVAL_FLIGHT_COUNT;
	}


	public double getServiceIntervalFlightHours() {
		return SERVICE_INTERVAL_FLIGHT_HOURS;
	}
	
	public double setLastSPFlightCount(double lastSPCount){
		lastSPCount = lastSPFlightCount;
		return lastSPFlightCount;
	}
	
	public double setLastSPFlightHours(double lastSPHours){
		lastSPHours = lastSPFlightHours;
		return lastSPFlightHours;
	}


	@Override
	public void service() {
		

	}

	@Override
	public boolean wouldExceedServicePoint(double distToTravel) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public String toString(){
		return "Last Flight Count Service Point = " + lastSPFlightCount + ", Last Flight Hours Service Point = " + lastSPFlightHours
				+ ", Flight Count Service Interval = " + SERVICE_INTERVAL_FLIGHT_COUNT + ", Flight Hours Service Interval = " + 
				SERVICE_INTERVAL_FLIGHT_HOURS + ", Aircraft = " + aircraft.toString();
	}

}
