package fleet.model;

public class Van extends LandVehicle {
	
	public static final double WEAR_AND_TEAR_RATE = 0.6;
	public Van(String make, String model, String regNo, int year, double serviceInterval) {
		super(make, model, regNo, year, serviceInterval);
	}
	
	public double calculateWearAndTear(double distance){
		return distance*WEAR_AND_TEAR_RATE;
	}

	@Override
	public String toString() {
		return "make = " + getMake() + ", model = " + getModel() + ", year = " + getYear() + ", regNo = " + getRegNo() + ", odometer = " + getOdometer();
	}
	
	

}
