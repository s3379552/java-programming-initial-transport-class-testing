package fleet.model;

public interface Vehicle {

	public double calculateWearAndTear(double distance);
	
	public String getRegNo();
	
	public void service();
	
	public boolean canTravel(double distance);
	
	public double travel(double distance);
}
