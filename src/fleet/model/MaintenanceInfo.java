package fleet.model;

public interface MaintenanceInfo {

	public void service();
	
	public abstract boolean wouldExceedServicePoint(double distToTravel);
}
