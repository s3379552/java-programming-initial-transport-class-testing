package fleet.model;

public class LandMaintenanceInfo implements MaintenanceInfo {

	private double lastServicePoint;
	private final double SERVICE_INTERVAL;
	private LandVehicle landVehicle;
	public LandMaintenanceInfo(LandVehicle vehicle, double serviceInterval){
		this.landVehicle = vehicle;
		this.lastServicePoint = 0.0;
		SERVICE_INTERVAL = serviceInterval;
		landVehicle = vehicle;
	}

	public double getLastServicePoint(){
		return lastServicePoint;
	}

	public double getNextServicePoint(){
		return lastServicePoint + SERVICE_INTERVAL;
	}

	public double getServiceInterval(){
		return SERVICE_INTERVAL;
	}

	public LandVehicle getVehicle(){
		return landVehicle;
	}

	public void service(){
		lastServicePoint = landVehicle.getOdometer();
	}

	public boolean wouldExceedServicePoint(double distToTravel){
		if((distToTravel + landVehicle.getOdometer()) > getNextServicePoint())
			return true;
		return false;
	}

	public String toString() {
		return "Last Service Point = " + lastServicePoint + ", Service Interval = " + SERVICE_INTERVAL + ", Vehicle = "
				+ landVehicle.toString();
	}



}
