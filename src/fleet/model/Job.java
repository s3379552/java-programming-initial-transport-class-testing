package fleet.model;

public class Job {

	private double cost;
	private double distance;
	private String jobID;
	private Vehicle vehicle;
	public Job(double cost, double distance, String jobID, Vehicle vehicle) {
		this.cost = cost;
		this.distance = distance;
		this.jobID = jobID;
		this.vehicle = vehicle;
	}
	
	public double getCost(){
		return cost;
	}
	
	public Vehicle getVehicle(){
		return vehicle;
	}
	
	public Object getJobID(){
		return jobID;
	}
	
	public String toString(){
		return "JobID = " + getJobID() + ", cost = " + getCost() + ", Vehicle = " + getVehicle();
	}
}
